# About

This is the code for my Master's Thesis "Noise Reduction for Interactive Volume Visualization Using Volumetric Path Tracing". It extends the basic volume renderer by adding volumetric path tracing using stochastic Monte Carlo methods.

<p align="center">
  <img src="img/DVRVergleich2.png" width="700" title="Comparison DVR and VPT">
</p>

Additionally, various post-processing techniques using spatial and temporal filtering were implemented. Temporal anti-aliasing approaches for volumes as well as linear models using both a geometry-based and a temporal feature are explored. The regression-based method was improved by employing a modified feature with masking and applying a percentile filter instead of a bilateral filter. Furthermore, different techniques to construct the depth buffer (first hit, DVR depth, representative), which is used for temporal reprojection, and a feature considering non-linear dependencies were added.

The mentioned techniques are mainly based on these two papers:

J. A. Iglesias-Guitian, P. S. Mane and B. Moon, "Real-Time Denoising of Volumetric Path Tracing for Direct Volume Rendering," in IEEE Transactions on Visualization and Computer Graphics, doi: https://doi.org/10.1109/TVCG.2020.3037680

J. Martschinke, S. Hartnagel, B. Keinert, K. Engel, M. Stamminger, "Adaptive Temporal Sampling for Volumetric Path Tracing of Medical Data" in The Eurographics Association and John Wiley & Sons Ltd., doi: https://doi.org/10.1111/cgf.13771 

<p align="center">
  <img src="img/ejecta.png" width="300" title="Ejecta Denoising">
  <img src="img/transferFunctions.png" width="300" title="Ejecta Denoising">
</p>

The evaluation of the different approaches can be found at: https://gitlab.com/TheOwlforge/mt_data

The final thesis can be found at: https://www.overleaf.com/read/kmdvkdxmgbgw
