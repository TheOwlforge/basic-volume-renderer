#pragma once

#include <cuda_runtime.h>
#include <GL/glew.h>
#include <cuMat/src/Matrix.h>

#include "commons.h"
#include "settings.h"
#include "renderer.h"
#include "warping.h"


BEGIN_RENDERER_NAMESPACE

struct MY_API Denoiser
{
	typedef cuMat::Matrix<float, cuMat::Dynamic, cuMat::Dynamic, cuMat::Dynamic, cuMat::ColumnMajor> DenoisingTensor;

	static void initDenoising(
		DenoisingTensor& weights,
		DenoisingTensor& covariance,
		DenoisingTensor& taa_weights,
		const renderer::OutputTensor& currentImage,
		const int regressorLength,
		const bool nonlinear);

	static void denoise(
		renderer::OutputTensor& inputTensor,
		const renderer::OutputTensor& previousInput,
		const renderer::DenoisingSettings& s,
		DenoisingTensor& weights,
		DenoisingTensor& covariance);

	static void filter(
		renderer::OutputTensor& inputTensor,
		const renderer::DenoisingSettings& s);
};

END_RENDERER_NAMESPACE