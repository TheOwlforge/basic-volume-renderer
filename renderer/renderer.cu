#include "renderer.h"
#include <random>
#include <iomanip>
#include <cuMat/src/Errors.h>
#include <cuMat/src/Context.h>
#include <cuMat/src/SimpleRandom.h>

#include "helper_math.h"
#include "camera.h"
#include "tf_texture_1d.h"
#include "errors.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif


namespace std
{
	std::ostream& operator<<(std::ostream& o, const float3& v)
	{
		o << std::fixed << std::setw(5) << std::setprecision(3)
			<< v.x << "," << v.y << "," << v.z;
		return o;
	}
	std::ostream& operator<<(std::ostream& o, const float4& v)
	{
		o << std::fixed << std::setw(5) << std::setprecision(3)
			<< v.x << "," << v.y << "," << v.z << "," << v.w;
		return o;
	}
}

BEGIN_RENDERER_NAMESPACE

//=========================================
// HELPER 
//=========================================

std::tuple<std::vector<float4>, std::vector<float4>> computeAmbientOcclusionParameters(int samples, int rotations)
{
	static std::default_random_engine rnd;
	static std::uniform_real_distribution<float> distr(0.0f, 1.0f);
	//samples
	std::vector<float4> aoHemisphere(samples);
	for (int i = 0; i < samples; ++i)
	{
		float u1 = distr(rnd);
		float u2 = distr(rnd);
		float r = std::sqrt(u1);
		float theta = 2 * M_PI * u2;
		float x = r * std::cos(theta);
		float y = r * std::sin(theta);
		float scale = distr(rnd);
		scale = 0.1 + 0.9 * scale * scale;
		aoHemisphere[i] = make_float4(x * scale, y * scale, std::sqrt(1 - u1) * scale, 0);
	}
	//random rotation vectors
	std::vector<float4> aoRandomRotations(rotations * rotations);
	for (int i = 0; i < rotations * rotations; ++i)
	{
		float x = distr(rnd) * 2 - 1;
		float y = distr(rnd) * 2 - 1;
		float linv = 1.0f / sqrt(x * x + y * y);
		aoRandomRotations[i] = make_float4(x * linv, y * linv, 0, 0);
	}

	return std::make_tuple(aoHemisphere, aoRandomRotations);
}

//=========================================
// RENDERER CONFIGURATION
//=========================================

#define MAX_AMBIENT_OCCLUSION_SAMPLES 512
__constant__ float4 aoHemisphere[MAX_AMBIENT_OCCLUSION_SAMPLES];
//#define AMBIENT_OCCLUSION_RANDOM_ROTATIONS 4
#define AMBIENT_OCCLUSION_RANDOM_ROTATIONS 1
__constant__ float4 aoRandomRotations[AMBIENT_OCCLUSION_RANDOM_ROTATIONS * AMBIENT_OCCLUSION_RANDOM_ROTATIONS];

struct RendererDeviceSettings
{
	float2 screenSize;
	float3 volumeSize;
	int binarySearchSteps;
	float stepsize;
	float normalStepSize;
	float3 eyePos;
	float4 currentViewMatrixInverse[4]; //row-by-row
	float4 currentViewMatrix[4];
	float4 nextViewMatrix[4];
	float4 normalMatrix[4];
	float3 boxMin;
	float3 boxSize;
	int aoSamples;
	float aoRadius;
	float aoBias;
	int4 viewport;
	float isovalue;
	float opacityScaling{ 1.0f };
	float minDensity{ 0.0f };
	float maxDensity{ 1.0f };

	//VPT
	float3 lightPosition;
	float lightRadius;
	bool directional = false;
	float albedo = 0.96f;
	float maxSigmaT = 1.0f;
	int depthMode = 0;
	bool mask = false;

	bool useShading = false;
	ShadingSettings shading;
};

//texture<float, 3, cudaReadModeElementType> float_tex;
//texture<unsigned char, 3, cudaReadModeNormalizedFloat> char_tex;
//texture<unsigned short, 3, cudaReadModeNormalizedFloat> short_tex;

//=========================================
// RENDERER KERNEL
//=========================================

inline __device__ float4 matmul(const float4 mat[4], float4 v)
{
	return make_float4(
		dot(mat[0], v),
		dot(mat[1], v),
		dot(mat[2], v),
		dot(mat[3], v)
	);
}

__device__ inline void writeOutputIso(
	OutputTensor& output, int x, int y,
	float mask = 0, float3 normal = { 0,0,0 }, float depth = 0, float ao = 1, float2 flow = { 0,0 })
{
	output.coeff(y, x, 0, -1) = mask;
	output.coeff(y, x, 1, -1) = normal.x;
	output.coeff(y, x, 2, -1) = normal.y;
	output.coeff(y, x, 3, -1) = normal.z;
	output.coeff(y, x, 4, -1) = depth;
	output.coeff(y, x, 5, -1) = ao;
	output.coeff(y, x, 6, -1) = flow.x;
	output.coeff(y, x, 7, -1) = flow.y;
}

__device__ inline void writeOutputDvr(
	OutputTensor& output, int x, int y,
	const float3& rgb = { 0,0,0 }, float alpha = 0,
	const float3& normal = { 0,0,0 }, float depth = 0, const float2& flow = { 0,0 })
{
	output.coeff(y, x, 0, -1) = rgb.x;
	output.coeff(y, x, 1, -1) = rgb.y;
	output.coeff(y, x, 2, -1) = rgb.z;
	output.coeff(y, x, 3, -1) = alpha;
	output.coeff(y, x, 4, -1) = normal.x;
	output.coeff(y, x, 5, -1) = normal.y;
	output.coeff(y, x, 6, -1) = normal.z;
	output.coeff(y, x, 7, -1) = depth;
	output.coeff(y, x, 8, -1) = flow.x;
	output.coeff(y, x, 9, -1) = flow.y;
}

__device__ inline void readOutputDvr(
	OutputTensor& output, int x, int y,
	float3& rgb, float& alpha,
	float3& normal, float& depth, float2& flow)
{
	rgb.x = output.coeff(y, x, 0, -1);
	rgb.y = output.coeff(y, x, 1, -1);
	rgb.z = output.coeff(y, x, 2, -1);
	alpha = output.coeff(y, x, 3, -1);
	normal.x = output.coeff(y, x, 4, -1);
	normal.y = output.coeff(y, x, 5, -1);
	normal.z = output.coeff(y, x, 6, -1);
	depth = output.coeff(y, x, 7, -1);
	flow.x = output.coeff(y, x, 8, -1);
	flow.y = output.coeff(y, x, 9, -1);
}

__device__ inline void writeOutputVpt(
	OutputTensor& output, int x, int y,
	const float3& rgb = { 0,0,0 }, float alpha = 0,
	const float3& normal = { 0,0,0 }, float depth = 0, const float2& flow = { 0,0 },
	const float bounces = 0, const float3& rayDir = {0,0,0})
{
	output.coeff(y, x, 0, -1) = rgb.x;
	output.coeff(y, x, 1, -1) = rgb.y;
	output.coeff(y, x, 2, -1) = rgb.z;
	output.coeff(y, x, 3, -1) = alpha;
	output.coeff(y, x, 4, -1) = normal.x;
	output.coeff(y, x, 5, -1) = normal.y;
	output.coeff(y, x, 6, -1) = normal.z;
	output.coeff(y, x, 7, -1) = depth;
	output.coeff(y, x, 8, -1) = flow.x;
	output.coeff(y, x, 9, -1) = flow.y;
	output.coeff(y, x, 10, -1) = bounces;
	output.coeff(y, x, 11, -1) = rayDir.x;
	output.coeff(y, x, 12, -1) = rayDir.y;
	output.coeff(y, x, 13, -1) = rayDir.z;
}

__device__ inline void readOutputVpt(
	OutputTensor& output, int x, int y,
	float3& rgb, float& alpha,
	float3& normal, float& depth, float2& flow,
	float& bounces, float3& rayDir)
{
	rgb.x = output.coeff(y, x, 0, -1);
	rgb.y = output.coeff(y, x, 1, -1);
	rgb.z = output.coeff(y, x, 2, -1);
	alpha = output.coeff(y, x, 3, -1);
	normal.x = output.coeff(y, x, 4, -1);
	normal.y = output.coeff(y, x, 5, -1);
	normal.z = output.coeff(y, x, 6, -1);
	depth = output.coeff(y, x, 7, -1);
	flow.x = output.coeff(y, x, 8, -1);
	flow.y = output.coeff(y, x, 9, -1);
	bounces = output.coeff(y, x, 10, -1);
	rayDir.x = output.coeff(y, x, 11, -1);
	rayDir.y = output.coeff(y, x, 12, -1);
	rayDir.z = output.coeff(y, x, 13, -1);
}

__device__ float customTex3D(cudaTextureObject_t tex, float x, float y, float z,
	std::integral_constant<int, RendererArgs::VolumeFilterMode::TRILINEAR>)
{
	return tex3D<float>(tex, x, y, z);
}

//Source: https://github.com/DannyRuijters/CubicInterpolationCUDA
// Inline calculation of the bspline convolution weights, without conditional statements
template<class T> inline __device__ void bspline_weights(T fraction, T& w0, T& w1, T& w2, T& w3)
{
	const T one_frac = 1.0f - fraction;
	const T squared = fraction * fraction;
	const T one_sqd = one_frac * one_frac;

	w0 = 1.0f / 6.0f * one_sqd * one_frac;
	w1 = 2.0f / 3.0f - 0.5f * squared * (2.0f - fraction);
	w2 = 2.0f / 3.0f - 0.5f * one_sqd * (2.0f - one_frac);
	w3 = 1.0f / 6.0f * squared * fraction;
}
//Source: https://github.com/DannyRuijters/CubicInterpolationCUDA
//TODO: change to texture object API to support char, short and float textures
__device__ float customTex3D(cudaTextureObject_t tex, float x, float y, float z,
	std::integral_constant<int, RendererArgs::VolumeFilterMode::TRICUBIC>)
{
	const float3 coord = make_float3(x, y, z);
	// shift the coordinate from [0,extent] to [-0.5, extent-0.5]
	const float3 coord_grid = coord - 0.5f;
	const float3 index = floorf(coord_grid);
	const float3 fraction = coord_grid - index;
	float3 w0, w1, w2, w3;
	bspline_weights(fraction, w0, w1, w2, w3);

	const float3 g0 = w0 + w1;
	const float3 g1 = w2 + w3;
	const float3 h0 = (w1 / g0) - 0.5f + index;  //h0 = w1/g0 - 1, move from [-0.5, extent-0.5] to [0, extent]
	const float3 h1 = (w3 / g1) + 1.5f + index;  //h1 = w3/g1 + 1, move from [-0.5, extent-0.5] to [0, extent]

	// fetch the eight linear interpolations
	// weighting and fetching is interleaved for performance and stability reasons
	typedef float floatN; //return type
	floatN tex000 = tex3D<float>(tex, h0.x, h0.y, h0.z);
	floatN tex100 = tex3D<float>(tex, h1.x, h0.y, h0.z);
	tex000 = g0.x * tex000 + g1.x * tex100;  //weigh along the x-direction
	floatN tex010 = tex3D<float>(tex, h0.x, h1.y, h0.z);
	floatN tex110 = tex3D<float>(tex, h1.x, h1.y, h0.z);
	tex010 = g0.x * tex010 + g1.x * tex110;  //weigh along the x-direction
	tex000 = g0.y * tex000 + g1.y * tex010;  //weigh along the y-direction
	floatN tex001 = tex3D<float>(tex, h0.x, h0.y, h1.z);
	floatN tex101 = tex3D<float>(tex, h1.x, h0.y, h1.z);
	tex001 = g0.x * tex001 + g1.x * tex101;  //weigh along the x-direction
	floatN tex011 = tex3D<float>(tex, h0.x, h1.y, h1.z);
	floatN tex111 = tex3D<float>(tex, h1.x, h1.y, h1.z);
	tex011 = g0.x * tex011 + g1.x * tex111;  //weigh along the x-direction
	tex001 = g0.y * tex001 + g1.y * tex011;  //weigh along the y-direction

	return (g0.z * tex000 + g1.z * tex001);  //weigh along the z-direction
}

__device__ void intersectionRayAABB(
	const float3& rayStart, const float3& rayDir,
	const float3& boxMin, const float3& boxSize,
	float& tmin, float& tmax)
{
	float3 invRayDir = 1.0f / rayDir;
	float t1 = (boxMin.x - rayStart.x) * invRayDir.x;
	float t2 = (boxMin.x + boxSize.x - rayStart.x) * invRayDir.x;
	float t3 = (boxMin.y - rayStart.y) * invRayDir.y;
	float t4 = (boxMin.y + boxSize.y - rayStart.y) * invRayDir.y;
	float t5 = (boxMin.z - rayStart.z) * invRayDir.z;
	float t6 = (boxMin.z + boxSize.z - rayStart.z) * invRayDir.z;
	tmin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
	tmax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));
}

template<int FilterMode>
__device__ float computeAmbientOcclusion(
	cudaTextureObject_t volume_tex, float3 pos, float3 normal,
	RendererDeviceSettings settings,
	int x, int y)
{
	if (settings.aoSamples == 0) return 1;
	float ao = 0.0;
	//get random rotation vector
	int x2 = x % AMBIENT_OCCLUSION_RANDOM_ROTATIONS;
	int y2 = y % AMBIENT_OCCLUSION_RANDOM_ROTATIONS;
	float3 noise = make_float3(aoRandomRotations[x2 + AMBIENT_OCCLUSION_RANDOM_ROTATIONS * y2]);
	//compute transformation
	float3 tangent = normalize(noise - normal * dot(noise, normal));
	float3 bitangent = cross(normal, tangent);
	//sample
	const float bias = settings.isovalue;//customTex3D(volume_tex, pos.x, pos.y, pos.z, std::integral_constant<int, FilterMode>());
	for (int i = 0; i < settings.aoSamples; ++i)
	{
		//get hemisphere sample
		float3 sampleT = normalize(make_float3(aoHemisphere[i]));
		//transform to world space
		float3 sampleW = make_float3(
			dot(make_float3(tangent.x, bitangent.x, normal.x), sampleT),
			dot(make_float3(tangent.y, bitangent.y, normal.y), sampleT),
			dot(make_float3(tangent.z, bitangent.z, normal.z), sampleT)
		);
		//shoot ray
		float tmin, tmax;
		intersectionRayAABB(pos, sampleW, settings.boxMin, settings.boxSize, tmin, tmax);
		assert(tmax > 0 && tmin < tmax);
		tmax = min(tmax, settings.aoRadius);
		float value = 1.0;
		for (float sampleDepth = settings.stepsize; sampleDepth <= tmax; sampleDepth += settings.stepsize)
		{
			float3 npos = pos + sampleDepth * sampleW;
			float3 volPos = (npos - settings.boxMin) / settings.boxSize * settings.volumeSize;
			float nval = customTex3D(volume_tex, volPos.x, volPos.y, volPos.z, std::integral_constant<int, FilterMode>());
			if (nval > bias)
			{
				value = .0f;//smoothstep(1, 0, settings.aoRadius / sampleDepth);
				break;
			}
		}
		ao += value;
	}
	return ao / settings.aoSamples;
}

struct RaytraceIsoOutput
{
	float3 posWorld;
	float3 normalWorld;
	float ao;
};
struct RaytraceDvrOutput
{
	float3 color;
	float alpha;
	float3 normalWorld;
	float depth;
};
struct VPTOutput
{
	float3 Lv;
	float3 transmittance;
	float3 weight;
	float3 normalWorld;
	float depth;
};

/**
 * The raytracing kernel for isosurfaces.
 *
 * Input:
 *  - screenPos: integer screen position, needed for AO sampling
 *  - settings: additional render settings
 *  - volume_tex: the 3D volume texture
 *
 * Input: rayStart, rayDir, tmin, tmax.
 * The ray enters the volume at rayStart + tmin*rayDir
 * and leaves the volume at rayStart + tmax*rayDir.
 *
 * Output:
 * Return 'true' if an intersection with the isosurface was found.
 * Then fill 'out' with the position, normal and AO at that position.
 * Else, return 'false'.
 */
template<int FilterMode>
__device__ __inline__ bool RaytraceKernel_Isosurface(
	int2 screenPos,
	RendererDeviceSettings settings, cudaTextureObject_t volume_tex,
	const float3& rayStart, const float3& rayDir, float tmin, float tmax,
	RaytraceIsoOutput& out)
{
	bool found = false;
	float3 pos = make_float3(0, 0, 0);
	float3 normal = make_float3(0, 0, 0);
	for (float sampleDepth = max(0.0f, tmin); sampleDepth < tmax && !found; sampleDepth += settings.stepsize)
	{
		float3 npos = settings.eyePos + sampleDepth * rayDir;
		float3 volPos = (npos - settings.boxMin) / settings.boxSize * settings.volumeSize;
		float nval = customTex3D(volume_tex, volPos.x, volPos.y, volPos.z, std::integral_constant<int, FilterMode>());
		if (nval > settings.isovalue)
		{
			found = true;
			//TODO: binary search
			//set position to the previous position for AO (slightly outside)
			pos = settings.eyePos + (sampleDepth - settings.stepsize) * rayDir;
			normal.x = 0.5 * (customTex3D(volume_tex, volPos.x + settings.normalStepSize, volPos.y, volPos.z, std::integral_constant<int, FilterMode>())
				- customTex3D(volume_tex, volPos.x - settings.normalStepSize, volPos.y, volPos.z, std::integral_constant<int, FilterMode>()));
			normal.y = 0.5 * (customTex3D(volume_tex, volPos.x, volPos.y + settings.normalStepSize, volPos.z, std::integral_constant<int, FilterMode>())
				- customTex3D(volume_tex, volPos.x, volPos.y - settings.normalStepSize, volPos.z, std::integral_constant<int, FilterMode>()));
			normal.z = 0.5 * (customTex3D(volume_tex, volPos.x, volPos.y, volPos.z + settings.normalStepSize, std::integral_constant<int, FilterMode>())
				- customTex3D(volume_tex, volPos.x, volPos.y, volPos.z - settings.normalStepSize, std::integral_constant<int, FilterMode>()));
			normal = -normal;
		}
	}
	if (found)
	{
		normal = safeNormalize(normal);
		out.posWorld = pos;
		out.normalWorld = normal;

		out.ao = computeAmbientOcclusion<FilterMode>(
			volume_tex, pos - settings.aoBias * rayDir, normal, settings, screenPos.x, screenPos.y);
	}
	return found;
}


/**
 * The raytracing kernel for DVR (Direct Volume Rendering).
 *
 * Input:
 *  - settings: additional render settings
 *  - volumeTex: the 3D volume texture
 *  - tfTexture: transfer function texture
 *
 * Input: rayStart, rayDir, tmin, tmax.
 * The ray enters the volume at rayStart + tmin*rayDir
 * and leaves the volume at rayStart + tmax*rayDir.
 *
 * Output:
 * Returns RGBA value accumulated through the ray direction.
 */
template<int FilterMode>
__device__ __inline__ RaytraceDvrOutput RaytraceKernel_Dvr(
	const RendererDeviceSettings& settings, cudaTextureObject_t volumeTex, cudaTextureObject_t tfTexture,
	const float3& rayStart, const float3& rayDir, float tmin, float tmax)
{
	auto rgbBuffer = make_float3(0.0f, 0.0f, 0.0f);
	auto oBuffer = 0.0f;
	auto normalBuffer = make_float3(0.0f, 0.0f, 0.0f);
	auto depthBuffer = 0.0f;
	for (float sampleDepth = max(0.0f, tmin); sampleDepth < tmax && oBuffer < 0.999f; sampleDepth += settings.stepsize)
	{
		float3 npos = settings.eyePos + sampleDepth * rayDir;
		float3 volPos = (npos - settings.boxMin) / settings.boxSize * settings.volumeSize;
		//nval = density
		float nval = customTex3D(volumeTex, volPos.x, volPos.y, volPos.z, std::integral_constant<int, FilterMode>());

		if (nval >= settings.minDensity && nval <= settings.maxDensity)
		{
			nval = (nval - settings.minDensity) / (settings.maxDensity - settings.minDensity);
		}
		else
		{
			continue;
		}

		auto rgba = tex1D<float4>(tfTexture, nval);
		auto opacity = rgba.w * settings.opacityScaling * settings.stepsize;
		opacity = min(1.0, opacity);

		if (opacity > 1e-4)
		{
			//compute normal
			float3 normal;
			normal.x = 0.5 * (customTex3D(volumeTex, volPos.x + settings.normalStepSize, volPos.y, volPos.z, std::integral_constant<int, FilterMode>())
				- customTex3D(volumeTex, volPos.x - settings.normalStepSize, volPos.y, volPos.z, std::integral_constant<int, FilterMode>()));
			normal.y = 0.5 * (customTex3D(volumeTex, volPos.x, volPos.y + settings.normalStepSize, volPos.z, std::integral_constant<int, FilterMode>())
				- customTex3D(volumeTex, volPos.x, volPos.y - settings.normalStepSize, volPos.z, std::integral_constant<int, FilterMode>()));
			normal.z = 0.5 * (customTex3D(volumeTex, volPos.x, volPos.y, volPos.z + settings.normalStepSize, std::integral_constant<int, FilterMode>())
				- customTex3D(volumeTex, volPos.x, volPos.y, volPos.z - settings.normalStepSize, std::integral_constant<int, FilterMode>()));
			normal = safeNormalize(normal);

			if (settings.useShading)
			{
				//perform phong shading
				float3 color = make_float3(0);
				float3 col = make_float3(rgba);
				color += settings.shading.ambientLightColor * col; //ambient light
				color += settings.shading.diffuseLightColor * col *
					abs(dot(normal, settings.shading.lightDirection)); //diffuse
				float3 reflect = 2 * dot(settings.shading.lightDirection, normal) *
					normal - settings.shading.lightDirection;
				color += settings.shading.specularLightColor * (
					((settings.shading.specularExponent + 2) / (2 * M_PI)) *
					pow(clamp(dot(reflect, rayDir), 0.0f, 1.0f), settings.shading.specularExponent));
				//set as final color, keep alpha
				rgba = make_float4(color, rgba.w);
			}

			rgbBuffer += (1.0f - oBuffer) * opacity * make_float3(rgba.x, rgba.y, rgba.z);
			normalBuffer += (1.0f - oBuffer) * opacity * normal;
			depthBuffer += (1.0f - oBuffer) * opacity * sampleDepth;
			oBuffer += (1.0f - oBuffer) * opacity;
		}
	}

	return { rgbBuffer, oBuffer, normalBuffer, depthBuffer };
}

template<int FilterMode>
__device__ __inline__ float RaytraceDepth(
	const RendererDeviceSettings& settings, cudaTextureObject_t volumeTex, cudaTextureObject_t tfTexture,
	const float3& rayStart, const float3& rayDir, float tmin, float tmax)
{
	float oBuffer = 0.0f;
	float depth = 0.0f;
	float maxOpacity = 0.0f;
	float maxDepth = 0.0f;
	bool threshold_reached = false;

	for (float sampleDepth = max(0.0f, tmin); sampleDepth < tmax && oBuffer < 0.999f; sampleDepth += settings.stepsize)
	{
		float3 npos = settings.eyePos + sampleDepth * rayDir;
		float3 volPos = (npos - settings.boxMin) / settings.boxSize * settings.volumeSize;
		float density = customTex3D(volumeTex, volPos.x, volPos.y, volPos.z, std::integral_constant<int, FilterMode>());

		if (density >= settings.minDensity && density <= settings.maxDensity)
		{
			density = (density - settings.minDensity) / (settings.maxDensity - settings.minDensity);
		}
		else
		{
			continue;
		}

		float4 rgba = tex1D<float4>(tfTexture, density);
		float opacity = rgba.w * settings.opacityScaling * settings.stepsize;
		opacity = min(1.0, opacity);

		if (opacity > 1e-4)
		{
			depth = sampleDepth;
			oBuffer += opacity;
			if (opacity > maxOpacity)
			{
				maxOpacity = opacity;
				maxDepth = sampleDepth;
			}
		}
		if (oBuffer >= 0.9f)
		{
			threshold_reached = true;
			break;
		}
	}

	return threshold_reached ? depth : maxDepth;
}

__device__ __inline__ bool intersectRayDisk(const float3& rayPos, const float3& rayDir, const float3& diskCenter, const float3& diskNormal, float radius, float3& isectP)
{
	bool intersection = false;

	float angle = dot(diskNormal, rayDir);
	float distance = 0;
	if (angle < 0)
	{
		float3 u = diskCenter - rayPos;
		distance = dot(u, diskNormal) / angle;
		if (distance >= 0)
		{
			isectP = rayPos + distance * rayDir;
			float3 v = diskCenter - isectP;
			float d_squared = dot(v, v);
			if (d_squared <= radius * radius)
			{
				intersection = true;
			}
		}
	}

	return intersection;
}

// taken from real time rendering book
__device__ __inline__ bool intersectRaySphere(const float3& rayPos, const float3& rayDir, const float3& sphereCenter, float radius, float& t1, float& t2)
{
	float3 l = sphereCenter - rayPos;
	float s = dot(l, rayDir);
	float l2 = dot(l, l);
	float r2 = radius * radius;
	if (s < 0 && l2 > r2)
		return false;

	float m2 = l2 - s * s;
	if (m2 > r2)
		return false;

	float q = sqrt(r2 - m2);
	if (l2 > r2)
	{
		t1 = s - q;
		t2 = s + q;
	}
	else
	{
		t1 = s + q;
		t2 = s - q;
	}

	return true;
}

// beta = 2 hardcoded
// taken from http://www.pbr-book.org/3ed-2018/Monte_Carlo_Integration/Importance_Sampling.html#PowerHeuristic
__device__ __inline__ float powerHeuristic(int nf, float fPdf, int ng, float gPdf)
{
	float f = nf * fPdf, g = ng * gPdf;
	return (f * f) / (f * f + g * g);
}

//sample a directional light
template<int FilterMode>
__device__ __inline__ float sampleDirectionalLight(const RendererDeviceSettings& settings, cudaTextureObject_t volumeTex, cudaTextureObject_t tfTexture,
	curandState* state, const float3& pos, float max_sigma_t,
	float3& lightDir, float3& L, float3& beamTransmittance)
{
	lightDir = -settings.shading.lightDirection;
	lightDir = safeNormalize(lightDir);
	L = settings.shading.diffuseLightColor;

	//ratio tracking for transmittance
	float tmin, tmax;
	intersectionRayAABB(pos, lightDir, settings.boxMin, settings.boxSize, tmin, tmax);
	float t = max(tmin, 0.0f);
	beamTransmittance = make_float3(1.0f);
	do
	{
		t -= logf(1 - curand_uniform(state)) / (max_sigma_t * settings.opacityScaling);
		if (t > tmax)
		{
			break;
		}

		float3 worldPos = pos + t * lightDir;
		float3 volPos = (worldPos - settings.boxMin) / settings.boxSize * settings.volumeSize;
		float density = customTex3D(volumeTex, volPos.x, volPos.y, volPos.z, std::integral_constant<int, FilterMode>());

		if (density < settings.minDensity || density > settings.maxDensity)
		{
			continue;
		}

		density = (density - settings.minDensity) / (settings.maxDensity - settings.minDensity); //normalization
		float4 rgba = tex1D<float4>(tfTexture, density);
		//beamTransmittance *= (1.0f - rgba.w / (max_sigma_t * settings.opacityScaling)));
		beamTransmittance *= (1.0f - rgba.w);

	} while (true);

	return 1.0f;
}

//directional light: pdf is zero due to delta distribution
__device__ __inline__ float evaluateDirectionalLight(const float3& dir, float3& L, float3& beamTransmittance)
{
	L.x = L.y = L.z = 0.0f;
	beamTransmittance = make_float3(0);
	return 0.0f;
}

//area light
//dir needs to be normalized
template<int FilterMode>
__device__ __inline__ float evaluateAreaLight(const RendererDeviceSettings& settings, cudaTextureObject_t volumeTex, cudaTextureObject_t tfTexture,
	curandState* state, const float3& dir, const float3& pos, float max_sigma_t,
	float3& L, float3& beamTransmittance)
{
	float3 center = settings.lightPosition;
	float3 normal = -center;
	normal = safeNormalize(normal);
	float radius = settings.lightRadius;

	// intersect ray with area
	float3 isectP;
	bool intersection = intersectRayDisk(pos, dir, center, normal, radius, isectP);

	if (!intersection)
	{
		L.x = L.y = L.z = 0.0f;
		return 0.0f;
	}

	// compute pdf wrt solid angle to pos
	L = settings.shading.diffuseLightColor;

	float3 v = pos - isectP;
	float d_squared = dot(v, v);
	float absDot = abs(dot(normal, dir));
	float area = radius * radius * M_PI;
	float pdf = d_squared / (absDot * area);

	//ratio tracking for transmittance
	float tmin, tmax;
	intersectionRayAABB(pos, dir, settings.boxMin, settings.boxSize, tmin, tmax);
	float t = max(tmin, 0.0f);
	beamTransmittance = make_float3(1.0f);
	do
	{
		t -= logf(1 - curand_uniform(state)) / (max_sigma_t * settings.opacityScaling);
		if (t > tmax)
		{
			break;
		}

		float3 worldPos = pos + t * dir;
		float3 volPos = (worldPos - settings.boxMin) / settings.boxSize * settings.volumeSize;
		float density = customTex3D(volumeTex, volPos.x, volPos.y, volPos.z, std::integral_constant<int, FilterMode>());

		if (density < settings.minDensity || density > settings.maxDensity)
		{
			continue;
		}

		density = (density - settings.minDensity) / (settings.maxDensity - settings.minDensity); //normalization
		float4 rgba = tex1D<float4>(tfTexture, density);
		beamTransmittance *= (1.0f - rgba.w);

	} while (true);

	return pdf;
}

template<int FilterMode>
__device__ __inline__ float sampleAreaLight(const RendererDeviceSettings& settings, cudaTextureObject_t volumeTex, cudaTextureObject_t tfTexture,
	curandState* state, const float3& pos, float max_sigma_t,
	float3& lightDir, float3& L, float3& beamTransmittance)
{
	float3 center = settings.lightPosition;
	float3 normal = -center;
	normal = safeNormalize(normal);
	float radius = settings.lightRadius;

	// concentric sample disk
	// adapted from http://www.pbr-book.org/3ed-2018/Monte_Carlo_Integration/2D_Sampling_with_Multidimensional_Transformations.html#ConcentricSampleDisk
	float2 rand = make_float2(curand_uniform(state) * 2.0f - 1.0f, curand_uniform(state) * 2.0f - 1.0f);
	float theta, r;
	if (abs(rand.x) > abs(rand.y)) {
		r = rand.x;
		theta = (M_PI / 4.0f) * (rand.y / rand.x);
	}
	else {
		r = rand.y;
		theta = (M_PI / 2.0f) - (M_PI / 4.0f) * (rand.x / rand.y);
	}
	float2 p = r * make_float2(cos(theta), sin(theta));

	//rotate sample point
	float a = -normal.z * p.x + normal.y * p.y;
	float b = -normal.x * p.y;
	float c = (normal.x * normal.x * p.x) + (normal.y * normal.y * p.x) + (normal.y * normal.z * p.y);
	float3 rotatedDir = make_float3(a, b, c);
	float3 pWorld = center + radius * rotatedDir;

	//compute light direction and pdf value
	lightDir = safeNormalize(pWorld - pos);

	return evaluateAreaLight<FilterMode>(settings, volumeTex, tfTexture, state, lightDir, pos, max_sigma_t, L, beamTransmittance);
}

__device__ __inline__ float samplePDF(curandState* state, float3& dir, float3& L)
{
	float cosTheta = curand_uniform(state) * 2 - 1;
	dir.z = cosTheta;
	float sinThetaSquared = 1 - cosTheta * cosTheta;
	if (sinThetaSquared > 0.0f)
	{
		float sinTheta = sqrt(sinThetaSquared);
		float phi = curand_uniform(state) * 2 * M_PI;
		dir.x = sinTheta * cosf(phi);
		dir.y = sinTheta * sinf(phi);
	}
	else
	{
		dir.x = dir.y = 0.0f;
	}
	float pdf = 0.25f / M_PI;
	L.x = L.y = L.z = pdf;
	return pdf;
}

__device__ __inline__ float evaluatePDF(curandState* state, const float3& dir, float3& L)
{
	float pdf = 0.25f / M_PI;
	L.x = L.y = L.z = pdf;
	return pdf;
}

template<int FilterMode>
__device__ __inline__ VPTOutput samplePathsegment(
	const RendererDeviceSettings& settings, cudaTextureObject_t volumeTex, cudaTextureObject_t tfTexture,
	const float3& rayStart, const float3& rayDir, float tmin, float tmax,
	curandState* state)
{
	float3 Lv = make_float3(0.0f, 0.0f, 0.0f);
	float3 transmittance = make_float3(1.0f, 1.0f, 1.0f);
	float3 weight = make_float3(1.0f, 1.0f, 1.0f);
	float3 normal = make_float3(0.0f, 0.0f, 0.0f);
	float depth = 0.0f;

	//float4 sigma_a = make_float4(0.1f, 0.1f, 0.1f, 0.1f);
	//float4 sigma_s = make_float4(0.4f, 0.4f, 0.4f, 0.4f);
	//float sigma_t = sigma_a.x + sigma_s.x;
	float max_sigma_t = settings.maxSigmaT;

	float sampleDepth = max(0.0f, tmin); //start at ray origin if inside volume or first intersection with volume if outside

	while (true)
	{
		float step = -logf(1 - curand_uniform(state)) / (max_sigma_t * settings.opacityScaling);
		sampleDepth += step;
		if (sampleDepth >= tmax) break; //exited volume

		float3 worldPos = rayStart + sampleDepth * rayDir;
		float3 volPos = (worldPos - settings.boxMin) / settings.boxSize * settings.volumeSize;
		float density = customTex3D(volumeTex, volPos.x, volPos.y, volPos.z, std::integral_constant<int, FilterMode>());

		if (density < settings.minDensity || density > settings.maxDensity)
		{
			continue;
		}

		density = (density - settings.minDensity) / (settings.maxDensity - settings.minDensity); //normalization
		float4 rgba = tex1D<float4>(tfTexture, density);

		if (rgba.w / max_sigma_t > curand_uniform(state))
		{
			float albedo = settings.albedo;
			float extinction = rgba.w;
			float scattering = albedo * extinction;
			float3 materialColor = make_float3(rgba.x, rgba.y, rgba.z);

			float3 dir = make_float3(0);
			float3 lightL = make_float3(0);
			float3 phaseL = make_float3(0);
			float3 beamTransmittance = make_float3(0);
			float lightPDF = 0, phasePDF = 0, MISweight = 0;

			//Sample light source with MIS
			if (settings.directional)
			{
				lightPDF = sampleDirectionalLight<FilterMode>(settings, volumeTex, tfTexture, state, worldPos, max_sigma_t, dir, lightL, beamTransmittance);
			}
			else
			{
				lightPDF = sampleAreaLight<FilterMode>(settings, volumeTex, tfTexture, state, worldPos, max_sigma_t, dir, lightL, beamTransmittance);
			}
			if (lightPDF > 0)
			{
				phasePDF = evaluatePDF(state, dir, phaseL);

				MISweight = powerHeuristic(1, lightPDF, 1, phasePDF);

				Lv += ((settings.opacityScaling * materialColor * lightL * phaseL * beamTransmittance * scattering * MISweight) / lightPDF);
				//Lv = beamTransmittance;
			}

			//Sample phase function with MIS
			phasePDF = samplePDF(state, dir, phaseL);
			if (phasePDF > 0)
			{
				if (settings.directional)
				{
					lightPDF = evaluateDirectionalLight(dir, lightL, beamTransmittance);
				}
				else
				{
					lightPDF = evaluateAreaLight<FilterMode>(settings, volumeTex, tfTexture, state, dir, worldPos, max_sigma_t, lightL, beamTransmittance);
				}

				if (lightPDF > 0)
				{
					MISweight = powerHeuristic(1, lightPDF, 1, phasePDF);
					Lv += ((settings.opacityScaling * materialColor * lightL * phaseL * beamTransmittance * scattering * MISweight) / phasePDF);
				}
			}

			transmittance = make_float3(0);
			weight = make_float3(rgba.w);

			//set depth
			depth = sampleDepth;

			//compute normal
			normal.x = 0.5 * (customTex3D(volumeTex, volPos.x + settings.normalStepSize, volPos.y, volPos.z, std::integral_constant<int, FilterMode>())
				- customTex3D(volumeTex, volPos.x - settings.normalStepSize, volPos.y, volPos.z, std::integral_constant<int, FilterMode>()));
			normal.y = 0.5 * (customTex3D(volumeTex, volPos.x, volPos.y + settings.normalStepSize, volPos.z, std::integral_constant<int, FilterMode>())
				- customTex3D(volumeTex, volPos.x, volPos.y - settings.normalStepSize, volPos.z, std::integral_constant<int, FilterMode>()));
			normal.z = 0.5 * (customTex3D(volumeTex, volPos.x, volPos.y, volPos.z + settings.normalStepSize, std::integral_constant<int, FilterMode>())
				- customTex3D(volumeTex, volPos.x, volPos.y, volPos.z - settings.normalStepSize, std::integral_constant<int, FilterMode>()));
			normal = safeNormalize(normal);

			break;
		}
	}

	return { Lv, transmittance, weight, normal, depth };
}


/**
 * The rendering kernel with the parallel loop over the pixels and output handling
 */
template<int FilterMode>
__global__ void VPTEstimate(dim3 virtual_size,
	RendererDeviceSettings settings, cudaTextureObject_t volumeTex, cudaTextureObject_t tfTexture,
	OutputTensor output, curandState* states)
{
	const int H = output.rows();
	const int W = output.cols();
	const int C = output.batches();

	const int FIRST_HIT = 0;
	const int RAY_TRACE = 1;
	const int REPRESENTATIVE = 2;

	CUMAT_KERNEL_2D_LOOP(x_, y_, virtual_size)
	{
		unsigned int idx = y_ * H + x_;
		curandState* state = &states[idx];

		int x = x_ + settings.viewport.x;
		int y = y_ + settings.viewport.y;
		float posx = ((x + curand_uniform(state)) / settings.screenSize.x) * 2 - 1;
		float posy = ((y + curand_uniform(state)) / settings.screenSize.y) * 2 - 1;

		//target world position
		float4 screenPos = make_float4(posx, posy, 0.9, 1);
		float4 worldPos = matmul(settings.currentViewMatrixInverse, screenPos);
		if (worldPos.w > 0)
			worldPos /= worldPos.w;

		//ray direction
		float3 rayDir = normalize(make_float3(worldPos) - settings.eyePos);

		//ray origin
		float3 rayOrigin = settings.eyePos;

		float3 color = make_float3(0.0f, 0.0f, 0.0f);
		float alpha = 0.0f;
		float depth = 0.0f;
		float3 normalWorld = make_float3(0.0f, 0.0f, 0.0f);
		float3 throughput = make_float3(1.0f, 1.0f, 1.0f);

		int MAX_PATHLENGTH = 5;
		int bounces = 0;

		float tmin, tmax;
		intersectionRayAABB(rayOrigin, rayDir, settings.boxMin, settings.boxSize, tmin, tmax);
		const auto out = RaytraceKernel_Dvr<FilterMode>(
			settings, volumeTex, tfTexture,
			rayOrigin, rayDir, tmin, tmax);

		if (!settings.mask || out.alpha > 0)
		{
			for (; bounces < MAX_PATHLENGTH; bounces++)
			{
				//intersect ray with scene and stop if no intersection
				float tmin, tmax;
				intersectionRayAABB(rayOrigin, rayDir, settings.boxMin, settings.boxSize, tmin, tmax);
				if (tmax < 0 || tmin > tmax)
				{
					break;
				}

				//sample ray and update color
				const auto temp = samplePathsegment<FilterMode>(
					settings, volumeTex, tfTexture,
					rayOrigin, rayDir, tmin, tmax, state);

				color += temp.Lv * temp.weight * throughput;
				throughput *= temp.transmittance;

				//set depth and normal
				if (bounces == 0)
				{
					if (settings.depthMode == FIRST_HIT)
					{
						depth = temp.depth;
						normalWorld = temp.normalWorld;
					}
					else if (settings.depthMode == RAY_TRACE)
					{
						depth = out.alpha > 1e-5 ? out.depth / out.alpha : 0;
						depth -= settings.stepsize;
						alpha = out.alpha;
						normalWorld = out.normalWorld;
					}
					else if (settings.depthMode == REPRESENTATIVE)
					{
						depth = RaytraceDepth<FilterMode>(
							settings, volumeTex, tfTexture,
							settings.eyePos, rayDir, tmin, tmax);
						normalWorld = temp.normalWorld;
					}
				}

				//update ray origin and direction
				rayOrigin = rayOrigin + temp.depth * rayDir;
				float3 L_temp = make_float3(0);
				samplePDF(state, rayDir, L_temp);
			}
		}

		//compute alpha based on transmittance
		if (settings.depthMode != RAY_TRACE) alpha = 1 - throughput.x;

		//evaluate flow and depth
		rayDir = normalize(make_float3(worldPos) - settings.eyePos); 	//reset ray direction
		float3 posWorld = settings.eyePos + depth * rayDir;
		float4 screenCurrent = matmul(settings.currentViewMatrix, make_float4(posWorld, 1.0));
		if (screenCurrent.w > 0)
			screenCurrent /= screenCurrent.w;
		float4 screenNext = matmul(settings.nextViewMatrix, make_float4(posWorld, 1.0));
		if (screenNext.w > 0)
			screenNext /= screenNext.w;
		float2 flow = alpha > 1e-5
			? 0.5f * make_float2(screenCurrent.x - screenNext.x, screenCurrent.y - screenNext.y)
			: make_float2(0, 0);
		float depthScreen = alpha > 1e-5 ? screenCurrent.z : 0.0f;

		//evaluate normal
		normalWorld = safeNormalize(normalWorld);
		float3 normalScreen = make_float3(matmul(settings.normalMatrix, make_float4(-normalWorld, 0)));

		writeOutputVpt(output, x_, y_,
			color, alpha, normalScreen, depthScreen, flow,
			(float)bounces/MAX_PATHLENGTH, rayDir);
	}
	CUMAT_KERNEL_2D_LOOP_END
}

/**
 * apply 5x5 separable gaussian kernel as a pixel reconstruction filter
 */
__global__ void VPTFilter(dim3 virtual_size, OutputTensor output)
{
	float filter[] = { 1, 4,  6,  4,  1,
					   4, 16, 24, 16, 4,
					   6, 24, 36, 24, 6,
					   4, 16, 24, 16, 4,
					   1, 4,  6,  4,  1 };

	CUMAT_KERNEL_2D_LOOP(x_, y_, virtual_size)
	{
		int x = x_ + 2;
		int y = y_ + 2;

		float3 color = make_float3(0);
		float3 normal = make_float3(0);
		float2 flow = make_float2(0);
		float3 rayDir = make_float3(0);
		float alpha, depth, bounces, oldAlpha, oldDepth, oldBounces;
		float3 oldColor = make_float3(0);
		float3 oldNormal = make_float3(0);
		float2 oldFlow = make_float2(0);
		float3 oldRayDir = make_float3(0);

		for (int j = -2; j <= 2; j++)
		{
			for (int i = -2; i <= 2; i++)
			{
				readOutputVpt(output, x + i, y + j, oldColor, oldAlpha, oldNormal, oldDepth, oldFlow, oldBounces, oldRayDir);
				int weight = filter[(j + 2) * 5 + i + 2];
				color += weight * oldColor;
				alpha += weight * oldAlpha;
				normal += weight * oldNormal;
				depth += weight * oldDepth;
				flow += weight * oldFlow;
				bounces += weight * oldBounces;
				rayDir += weight * oldRayDir;
			}
		}
		color /= 256;
		alpha /= 256;
		normal /= 256;
		depth /= 256;
		flow /= 256;
		bounces /= 256;
		rayDir /= 256;

		readOutputVpt(output, x, y, oldColor, oldAlpha, oldNormal, oldDepth, oldFlow, oldBounces, oldRayDir);

		writeOutputVpt(output, x, y, color, oldAlpha, oldNormal, oldDepth, oldFlow, oldBounces, oldRayDir);
	}
	CUMAT_KERNEL_2D_LOOP_END
}

/**
 * perform monte carlo integration by computing the cummulative moving average
 */
__global__ void VPTIntegrate(dim3 virtual_size, OutputTensor estimate, OutputTensor output, int numSPP)
{
	CUMAT_KERNEL_2D_LOOP(x_, y_, virtual_size)
	{
		float3 color = make_float3(0);
		float3 normal = make_float3(0);
		float2 flow = make_float2(0);
		float3 rayDir = make_float3(0);
		float alpha, depth, bounces, oldAlpha, oldDepth, oldBounces;
		float3 oldColor = make_float3(0);
		float3 oldNormal = make_float3(0);
		float2 oldFlow = make_float2(0);
		float3 oldRayDir = make_float3(0);

		readOutputVpt(estimate, x_, y_, color, alpha, normal, depth, flow, bounces, rayDir);
		readOutputVpt(output, x_, y_, oldColor, oldAlpha, oldNormal, oldDepth, oldFlow, oldBounces, oldRayDir);

		//compute cummulative moving average
		color = (color + numSPP * oldColor) / (numSPP + 1);
		alpha = (alpha + numSPP * oldAlpha) / (numSPP + 1);
		normal = (normal + numSPP * oldNormal) / (numSPP + 1);
		depth = (depth + numSPP * oldDepth) / (numSPP + 1);
		rayDir = (rayDir + numSPP * oldRayDir) / (numSPP + 1);

		writeOutputVpt(output, x_, y_, color, alpha, normal, depth, flow, bounces, rayDir);
	}
	CUMAT_KERNEL_2D_LOOP_END
}

/**
 * primitively draw area light for spacial orientation
 */
__global__ void VPTDrawLight(dim3 virtual_size, OutputTensor output, RendererDeviceSettings settings, float lat, float lon)
{
	CUMAT_KERNEL_2D_LOOP(x_, y_, virtual_size)
	{
		int x = x_ + settings.viewport.x;
		int y = y_ + settings.viewport.y;
		float posx = ((x + 0.5f) / settings.screenSize.x) * 2 - 1;
		float posy = ((y + 0.5f) / settings.screenSize.y) * 2 - 1;

		//target world position
		float4 screenPos = make_float4(posx, posy, 0.9, 1);
		float4 worldPos = matmul(settings.currentViewMatrixInverse, screenPos);
		worldPos /= worldPos.w;

		//ray direction
		float3 rayDir = normalize(make_float3(worldPos) - settings.eyePos);

		//ray origin
		float3 rayOrigin = settings.eyePos;

		float3 center, isectP;
		float radius;

		center = make_float3(0);
		radius = sqrt(dot(settings.lightPosition, settings.lightPosition));
		float t1, t2;
		if (intersectRaySphere(rayOrigin, rayDir, center, radius, t1, t2))
		{
			float theta, phi;

			isectP = rayOrigin + t2 * rayDir;
			theta = acos(isectP.z / radius);
			if (theta > lat - 1e-2 && theta < lat + 1e-2)
			{
				output.coeff(y_, x_, 0, -1) = 0;
				output.coeff(y_, x_, 1, -1) = 0.4f;
				output.coeff(y_, x_, 2, -1) = 0;
			}
			phi = atan2(isectP.y, isectP.x);
			if (phi > lon - 1e-2 && phi < lon + 1e-2)
			{
				output.coeff(y_, x_, 0, -1) = 0;
				output.coeff(y_, x_, 1, -1) = 0;
				output.coeff(y_, x_, 2, -1) = 1;
			}

			isectP = rayOrigin + t1 * rayDir;
			theta = acos(isectP.z / radius);
			if (theta > lat - 1e-2 && theta < lat + 1e-2)
			{
				output.coeff(y_, x_, 0, -1) = 0;
				output.coeff(y_, x_, 1, -1) = 0.8f;
				output.coeff(y_, x_, 2, -1) = 0;
			}
			phi = atan2(isectP.y, isectP.x);
			if (phi > lon - 1e-2 && phi < lon + 1e-2)
			{
				output.coeff(y_, x_, 0, -1) = 0;
				output.coeff(y_, x_, 1, -1) = 0;
				output.coeff(y_, x_, 2, -1) = 1;
			}
		}

		center = settings.lightPosition;
		radius = settings.lightRadius;
		float3 normal = -center;
		normal = safeNormalize(normal);
		if (intersectRayDisk(rayOrigin, rayDir, center, normal, radius, isectP) || intersectRayDisk(rayOrigin, rayDir, center, -normal, radius, isectP))
		{
			float3 rgb = settings.shading.diffuseLightColor;
			output.coeff(y_, x_, 0, -1) = rgb.x;
			output.coeff(y_, x_, 1, -1) = rgb.y;
			output.coeff(y_, x_, 2, -1) = rgb.z;
		}

		/*for (float lon = 0; lon < 2 * M_PI; lon += M_PI / 16) {
			center = make_float3(sinf(M_PI / 2) * cosf(lon), sinf(M_PI / 2) * sinf(lon), cosf(M_PI / 2));
			normal = -center;
			normal = safeNormalize(normal);
			radius = 0.01;
			if (intersectRayDisk(rayOrigin, rayDir, center, normal, radius, isectP) || intersectRayDisk(rayOrigin, rayDir, center, -normal, radius, isectP))
			{
				output.coeff(y_, x_, 0, -1) = 0;
				output.coeff(y_, x_, 1, -1) = 1;
				output.coeff(y_, x_, 2, -1) = 0;
			}
		}
		for (float lat = 0; lat < M_PI; lat += M_PI / 16) {
			center = make_float3(sinf(lat) * cosf(0), sinf(lat) * sinf(0), cosf(lat));
			normal = -center;
			normal = safeNormalize(normal);
			radius = 0.01;
			if (intersectRayDisk(rayOrigin, rayDir, center, normal, radius, isectP) || intersectRayDisk(rayOrigin, rayDir, center, -normal, radius, isectP))
			{
				output.coeff(y_, x_, 0, -1) = 0;
				output.coeff(y_, x_, 1, -1) = 0;
				output.coeff(y_, x_, 2, -1) = 1;
			}
		}*/
	}
	CUMAT_KERNEL_2D_LOOP_END
}

/**
 * The rendering kernel with the parallel loop over the pixels and output handling
 */
template<int FilterMode>
__global__ void IsosurfaceKernel(dim3 virtual_size,
	RendererDeviceSettings settings, cudaTextureObject_t volume_tex,
	OutputTensor output)
{
	CUMAT_KERNEL_2D_LOOP(x_, y_, virtual_size)

		int x = x_ + settings.viewport.x;
	int y = y_ + settings.viewport.y;
	float posx = ((x + 0.5f) / settings.screenSize.x) * 2 - 1;
	float posy = ((y + 0.5f) / settings.screenSize.y) * 2 - 1;

	//target world position
	float4 screenPos = make_float4(posx, posy, 0.9, 1);
	float4 worldPos = matmul(settings.currentViewMatrixInverse, screenPos);
	worldPos /= worldPos.w;

	//ray direction
	float3 rayDir = normalize(make_float3(worldPos) - settings.eyePos);

	//entry, exit points
	float tmin, tmax;
	intersectionRayAABB(settings.eyePos, rayDir, settings.boxMin, settings.boxSize, tmin, tmax);
	if (tmax < 0 || tmin > tmax)
	{
		writeOutputIso(output, x_, y_);
		continue;
	}

	//perform stepping
	RaytraceIsoOutput out;
	bool found = RaytraceKernel_Isosurface<FilterMode>(
		make_int2(x, y), settings, volume_tex,
		settings.eyePos, rayDir, tmin, tmax, out);

	if (found)
	{
		float4 screenCurrent = matmul(settings.currentViewMatrix, make_float4(out.posWorld, 1.0));
		screenCurrent /= screenCurrent.w;
		float4 screenNext = matmul(settings.nextViewMatrix, make_float4(out.posWorld, 1.0));
		screenNext /= screenNext.w;
		//evaluate depth and flow
		float mask = 1;
		float depth = screenCurrent.z;
		float2 flow = 0.5f * make_float2(screenCurrent.x - screenNext.x, screenCurrent.y - screenNext.y);
		float3 normalScreen = make_float3(matmul(settings.normalMatrix, make_float4(-out.normalWorld, 0)));
		//write output
		writeOutputIso(output, x_, y_, mask, normalScreen, depth, out.ao, flow);
	}
	else
	{
		writeOutputIso(output, x_, y_);
	}

	CUMAT_KERNEL_2D_LOOP_END
}

/**
 * The rendering kernel with the parallel loop over the pixels and output handling
 */
template<int FilterMode>
__global__ void DVRKernel(dim3 virtual_size,
	RendererDeviceSettings settings, cudaTextureObject_t volumeTex, cudaTextureObject_t tfTexture,
	OutputTensor output)
{
	CUMAT_KERNEL_2D_LOOP(x_, y_, virtual_size)
	{
		int x = x_ + settings.viewport.x;
		int y = y_ + settings.viewport.y;
		float posx = ((x + 0.5f) / settings.screenSize.x) * 2 - 1;
		float posy = ((y + 0.5f) / settings.screenSize.y) * 2 - 1;

		//target world position
		float4 screenPos = make_float4(posx, posy, 0.9, 1);
		float4 worldPos = matmul(settings.currentViewMatrixInverse, screenPos);
		worldPos /= worldPos.w;

		//ray direction
		float3 rayDir = normalize(make_float3(worldPos) - settings.eyePos);

		//entry, exit points
		float tmin, tmax;
		intersectionRayAABB(settings.eyePos, rayDir, settings.boxMin, settings.boxSize, tmin, tmax);
		if (tmax < 0 || tmin > tmax)
		{
			writeOutputDvr(output, x_, y_);
			continue;
		}

		const auto out = RaytraceKernel_Dvr<FilterMode>(
			settings, volumeTex, tfTexture,
			settings.eyePos, rayDir, tmin, tmax);

		//evaluate flow and depth
		float depth = out.alpha > 1e-5 ? out.depth / out.alpha : 0;
		float3 posWorld = settings.eyePos + (depth - settings.stepsize) * rayDir;
		float4 screenCurrent = matmul(settings.currentViewMatrix, make_float4(posWorld, 1.0));
		screenCurrent /= screenCurrent.w;
		float4 screenNext = matmul(settings.nextViewMatrix, make_float4(posWorld, 1.0));
		screenNext /= screenNext.w;
		float2 flow = out.alpha > 1e-5
			? 0.5f * make_float2(screenCurrent.x - screenNext.x, screenCurrent.y - screenNext.y)
			: make_float2(0, 0);
		float depthScreen = out.alpha > 1e-5 ? screenCurrent.z : 0.0f;
		//evaluate normal
		float3 normalWorld = safeNormalize(out.normalWorld);
		float3 normalScreen = make_float3(matmul(settings.normalMatrix, make_float4(-normalWorld, 0)));

		writeOutputDvr(output, x_, y_,
			out.color, out.alpha, normalScreen, depthScreen, flow);
	}
	CUMAT_KERNEL_2D_LOOP_END
}


//=========================================
// RENDERER_LAUNCHER
//=========================================

void render_gpu(const Volume* volume, const RendererArgs* args, OutputTensor& output, cudaStream_t stream, curandState* rng)
{
	CHECK_ERROR(output.cols() == args->cameraResolutionX,
		"Expected the number of columns in the output tensor (", output.cols(), ") to be equal to camere resolution along X (", args->cameraResolutionX, ")");
	CHECK_ERROR(output.rows() == args->cameraResolutionY,
		"Expected the number of rows in the output tensor (", output.rows(), ") to be equal to camere resolution along Y (", args->cameraResolutionY, ")");
	if (args->renderMode == RendererArgs::ISO) {
		CHECK_ERROR(output.batches() == IsoRendererOutputChannels,
			"Excepted the number of batches in the output tensor (", output.batches(), ") to be equal to IsoRendererOutputChannels (", IsoRendererOutputChannels, ")");
	}
	else if (args->renderMode == RendererArgs::DVR) {
		CHECK_ERROR(output.batches() == DvrRendererOutputChannels,
			"Excepted the number of batches in the output tensor (", output.batches(), ") to be equal to DvrRendererOutputChannels (", DvrRendererOutputChannels, ")");
	}
	else if (args->renderMode == RendererArgs::VPT) {
		CHECK_ERROR(output.batches() == VptRendererOutputChannels,
			"Excepted the number of batches in the output tensor (", output.batches(), ") to be equal to VptRendererOutputChannels (", VptRendererOutputChannels, ")");
	}

	const Volume::MipmapLevel* data = volume->getLevel(args->mipmapLevel);
	CHECK_ERROR(data != nullptr, "mipmap level must exist");

	//set settings
	RendererDeviceSettings s;
	s.screenSize = make_float2(args->cameraResolutionX, args->cameraResolutionY);
	s.volumeSize = make_float3(data->sizeX(), data->sizeY(), data->sizeZ());
	s.binarySearchSteps = args->binarySearchSteps;
	s.stepsize = args->stepsize / std::max({ data->sizeX(), data->sizeY(), data->sizeZ() });
	s.normalStepSize = 0.5f;
	s.boxSize = make_float3(
		volume->worldSizeX(),
		volume->worldSizeY(),
		volume->worldSizeZ());
	s.boxMin = make_float3(-s.boxSize.x / 2, -s.boxSize.y / 2, -s.boxSize.z / 2);
	s.isovalue = args->isovalue;
	s.aoBias = args->aoBias;
	s.aoRadius = args->aoRadius;
	s.aoSamples = args->aoSamples;
	s.eyePos = args->cameraOrigin;
	s.viewport = args->cameraViewport;
	if (s.viewport.z < 0) s.viewport.z = args->cameraResolutionX;
	if (s.viewport.w < 0) s.viewport.w = args->cameraResolutionY;
	Camera::computeMatrices(
		args->cameraOrigin, args->cameraLookAt, args->cameraUp,
		args->cameraFovDegrees, args->cameraResolutionX, args->cameraResolutionY, args->nearClip, args->farClip,
		s.currentViewMatrix, s.currentViewMatrixInverse, s.normalMatrix);
	static float4 lastViewMatrix[4] = {
		make_float4(1,0,0,0), make_float4(0,1,0,0),
		make_float4(0,0,1,0), make_float4(0,0,0,1) };
	memcpy(s.nextViewMatrix, lastViewMatrix, sizeof(float4) * 4);
	memcpy(lastViewMatrix, s.currentViewMatrix, sizeof(float4) * 4);
	s.opacityScaling = args->opacityScaling;
	s.minDensity = args->minDensity;
	s.maxDensity = args->maxDensity;
	float lat = args->lightLatitude + M_PI / 2;
	float lon = args->lightLongitude;
	s.lightPosition = make_float3(args->lightDistance * sin(lat) * cos(lon),
		args->lightDistance * sin(lat) * sin(lon),
		args->lightDistance * cos(lat));
	s.lightRadius = args->lightRadius;
	s.albedo = args->albedo;
	s.maxSigmaT = args->maxSigmaT;
	s.directional = args->directional;
	s.useShading = args->dvrUseShading;
	s.shading = args->shading;
	s.depthMode = args->depthMode;
	s.mask = args->mask;

	//launch kernel
	cuMat::Context& ctx = cuMat::Context::current();
	static TfTexture1D rendererTfTexture;

	switch (args->renderMode)
	{
	case renderer::RendererArgs::ISO:

		if (args->volumeFilterMode == RendererArgs::TRILINEAR) {
			cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(s.viewport.z - s.viewport.x, s.viewport.w - s.viewport.y, IsosurfaceKernel<RendererArgs::TRILINEAR>);
			IsosurfaceKernel<RendererArgs::TRILINEAR>
				<< < cfg.block_count, cfg.thread_per_block, 0, stream >> >
				(cfg.virtual_size, s, data->dataTexGpu(), output);
		}
		else if (args->volumeFilterMode == RendererArgs::TRICUBIC) {
			cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(s.viewport.z - s.viewport.x, s.viewport.w - s.viewport.y, IsosurfaceKernel<RendererArgs::TRICUBIC>);
			IsosurfaceKernel<RendererArgs::TRICUBIC>
				<< < cfg.block_count, cfg.thread_per_block, 0, stream >> >
				(cfg.virtual_size, s, data->dataTexGpu(), output);
		}
		break;

	case renderer::RendererArgs::DVR:

		rendererTfTexture.updateIfChanged(args->densityAxisOpacity, args->opacityAxis, args->densityAxisColor, args->colorAxis);

		if (args->volumeFilterMode == RendererArgs::TRILINEAR) {
			cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(s.viewport.z - s.viewport.x, s.viewport.w - s.viewport.y, DVRKernel<RendererArgs::TRILINEAR>);
			DVRKernel<RendererArgs::TRILINEAR>
				<< < cfg.block_count, cfg.thread_per_block, 0, stream >> >
				(cfg.virtual_size, s, data->dataTexGpu(), rendererTfTexture.getTextureObject(), output);
		}
		else if (args->volumeFilterMode == RendererArgs::TRICUBIC) {
			cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(s.viewport.z - s.viewport.x, s.viewport.w - s.viewport.y, DVRKernel<RendererArgs::TRICUBIC>);
			DVRKernel<RendererArgs::TRICUBIC>
				<< < cfg.block_count, cfg.thread_per_block, 0, stream >> >
				(cfg.virtual_size, s, data->dataTexGpu(), rendererTfTexture.getTextureObject(), output);
		}
		break;

	case renderer::RendererArgs::VPT:

		rendererTfTexture.updateIfChanged(args->densityAxisOpacity, args->opacityAxis, args->densityAxisColor, args->colorAxis);

		RENDERER_NAMESPACE::OutputTensor frameEstimate = RENDERER_NAMESPACE::OutputTensor(
			args->cameraResolutionY,
			args->cameraResolutionX,
			RENDERER_NAMESPACE::VptRendererOutputChannels);

		if (args->volumeFilterMode == RendererArgs::TRILINEAR)
		{
			cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(s.viewport.z - s.viewport.x, s.viewport.w - s.viewport.y, VPTEstimate<RendererArgs::TRILINEAR>);
			VPTEstimate<RendererArgs::TRILINEAR>
				<< < cfg.block_count, cfg.thread_per_block, 0, stream >> >
				(cfg.virtual_size, s, data->dataTexGpu(), rendererTfTexture.getTextureObject(), frameEstimate, rng);
		}
		else if (args->volumeFilterMode == RendererArgs::TRICUBIC)
		{
			cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(s.viewport.z - s.viewport.x, s.viewport.w - s.viewport.y, VPTEstimate<RendererArgs::TRICUBIC>);
			VPTEstimate<RendererArgs::TRICUBIC>
				<< < cfg.block_count, cfg.thread_per_block, 0, stream >> >
				(cfg.virtual_size, s, data->dataTexGpu(), rendererTfTexture.getTextureObject(), frameEstimate, rng);
		}

		CUMAT_SAFE_CALL(cudaDeviceSynchronize());
		cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(s.viewport.z - s.viewport.x - 4, s.viewport.w - s.viewport.y - 4, VPTFilter);
		VPTFilter
			<< < cfg.block_count, cfg.thread_per_block, 0, stream >> >
			(cfg.virtual_size, frameEstimate);
		CUMAT_SAFE_CALL(cudaDeviceSynchronize());

		if (args->resting)
		{
			cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(s.viewport.z - s.viewport.x, s.viewport.w - s.viewport.y, VPTIntegrate);
			VPTIntegrate
				<< < cfg.block_count, cfg.thread_per_block, 0, stream >> >
				(cfg.virtual_size, frameEstimate, output, args->numSPP);
		}
		else
		{
			output = frameEstimate;
		}

		if (args->showLight)
		{
			if (lon > M_PI) lon -= 2 * M_PI; //adjust for later use in draw light method
			cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(s.viewport.z - s.viewport.x, s.viewport.w - s.viewport.y, VPTDrawLight);
			VPTDrawLight
				<< < cfg.block_count, cfg.thread_per_block, 0, stream >> >
				(cfg.virtual_size, output, s, lat, lon);
		}

		break;
	}

	CUMAT_CHECK_ERROR();

	//CUMAT_SAFE_CALL(cudaDeviceSynchronize());
}

int64_t initializeRenderer()
{
	auto params = computeAmbientOcclusionParameters(MAX_AMBIENT_OCCLUSION_SAMPLES, AMBIENT_OCCLUSION_RANDOM_ROTATIONS);
	CUMAT_SAFE_CALL(cudaMemcpyToSymbol(aoHemisphere, std::get<0>(params).data(), sizeof(float4) * MAX_AMBIENT_OCCLUSION_SAMPLES));
	CUMAT_SAFE_CALL(cudaMemcpyToSymbol(aoRandomRotations, std::get<1>(params).data(), sizeof(float4) * AMBIENT_OCCLUSION_RANDOM_ROTATIONS * AMBIENT_OCCLUSION_RANDOM_ROTATIONS));
	return 1;
}

__global__ void InitRNGKernel(
	dim3 virtual_size,
	curandState* rngStates,
	const int W, const int H)
{
	CUMAT_KERNEL_2D_LOOP(x, y, virtual_size)
	{
		unsigned int idx = y * W + x;
		curand_init(1234 + idx, idx, 0, &rngStates[idx]);
	}
	CUMAT_KERNEL_2D_LOOP_END
}

void initRNG(curandState* rngStates, const int width, const int height)
{
	cuMat::Context& ctx = cuMat::Context::current();
	cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(width, height, InitRNGKernel);
	cudaStream_t stream = ctx.stream();

	InitRNGKernel <<< cfg.block_count, cfg.thread_per_block, 0, stream >>>
		(cfg.virtual_size,
		rngStates,
		width,
		height);
	CUMAT_CHECK_ERROR();
	CUMAT_SAFE_CALL(cudaDeviceSynchronize());
}

END_RENDERER_NAMESPACE
