#pragma once

#include "commons.h"
#include <string>
#include <cuda_runtime.h>
#include <vector>

#ifdef _WIN32
#pragma warning( push )
#pragma warning( disable : 4251) // dll export of STL types
#endif

BEGIN_RENDERER_NAMESPACE

struct MY_API ShadingSettings
{
	float3 ambientLightColor = make_float3(0.1f,0.1f,0.1f);
	float3 diffuseLightColor = make_float3(0.8f,0.8f,0.8f);
	float3 specularLightColor = make_float3(0.1f,0.1f,0.1f);
	float specularExponent = 16;
	float3 materialColor = make_float3(1.0f, 1.0f, 1.0f);
	float aoStrength = 0;

	///the light direction
	/// renderer: world space
	/// post-shading: screen space
	float3 lightDirection = make_float3(0,0,1);
};

struct MY_API DenoisingSettings
{
	bool feature = true;
	bool weight = true;
	bool nonlinear = false;
	float alpha = 0.75f;
	float h = 0.75f;
	float forgettingFactor = 0.998f;

	int filter_window = 5;

	float sigma_s = 4;
	float sigma_r = 0.2f;
	float percentile = 0.5f;
	float k = 0.45;

	enum DenoiseFilterMode
	{
		BILATERAL,
		NLMEANS,
		PERCENTILE,
		NONE,
		_DENOISE_FILTER_MODE_COUNT_
	};
	DenoiseFilterMode filterMode = BILATERAL;
};

struct MY_API RendererArgs
{
	//The mipmap level, 0 means the original level
	int mipmapLevel = 0;
	enum RenderMode
	{
		ISO, //mask, normal, depth, flow
		DVR, //mask(alpha), rgb
		VPT
	};
	RenderMode renderMode = ISO;
	
	int cameraResolutionX = 512;
	int cameraResolutionY = 512;
	double cameraFovDegrees = 45;
	//Viewport (startX, startY, endX, endY)
	//special values endX=endY=-1 delegate to cameraResolutionX and cameraResolutionY
	int4 cameraViewport = make_int4(0, 0, -1, -1);
	float3 cameraOrigin = make_float3(0, 0, -1);
	float3 cameraLookAt = make_float3(0, 0, 0);
	float3 cameraUp = make_float3(0, 1, 0);

	float nearClip = 0.1f;
	float farClip = 10.0f;

	double isovalue = 0.5;
	int binarySearchSteps = 5;
	double stepsize = 0.5;
	enum VolumeFilterMode
	{
		TRILINEAR,
		TRICUBIC,
		_VOLUME_FILTER_MODE_COUNT_
	};
	VolumeFilterMode volumeFilterMode = TRILINEAR;

	int aoSamples = 0;
	double aoRadius = 0.1;
	double aoBias = 1e-4;

	//TF Editor
	std::vector<float> densityAxisOpacity;
	std::vector<float> opacityAxis;
	std::vector<float> densityAxisColor;
	std::vector<float3> colorAxis;
	float opacityScaling = 1.0f;
	float minDensity = 0.0f;
	float maxDensity = 1.0f;

	//VPT
	int numSPP = 0;
	int depthMode = 0;
	bool resting = false;
	float lightLongitude = 0.0f;
	float lightLatitude = 0.0f;
	float lightDistance = 1.0f;
	float lightRadius = 0.2f;
	float albedo = 0.95f;
	float maxSigmaT = 1.0f;
	bool showLight = false;
	bool directional = false;
	bool mask = false;

	//shading
	ShadingSettings shading;
	bool dvrUseShading = false;
};
MY_API extern RendererArgs TheRendererArgs;

MY_API int64_t SetRendererParameter(const std::string& cmd, const std::string& value);

END_RENDERER_NAMESPACE

#ifdef _WIN32
#pragma warning( pop )
#endif
