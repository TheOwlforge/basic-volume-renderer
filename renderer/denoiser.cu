#include "denoiser.h"
#include "helper_math.h"

#ifndef EPSILON
#define EPSILON 1e-4
#endif
#ifndef TEMPORAL_FEATURE_LENGTH
#define TEMPORAL_FEATURE_LENGTH 3
#endif
#ifndef GBUFFER_FEATURE_LENGTH
#define GBUFFER_FEATURE_LENGTH 5
#endif
#ifndef NUM_COLOR_CHANNELS
#define NUM_COLOR_CHANNELS 3
#endif
#ifndef VARIANCE
#define VARIANCE 1000
#endif
#ifndef NONLINEAR_FAKTOR
#define NONLINEAR_FAKTOR 4
#endif


namespace
{
	__device__ inline void initWithZero(float* a, float length)
	{
		for (int i = 0; i < length; i++) a[i] = 0.0f;
	}

	__device__ inline float dotProduct(const float* vecA, const float* vecB, float length)
	{
		float dotProduct = 0;
		for (int i = 0; i < length; i++) dotProduct += vecA[i] * vecB[i];
		return dotProduct;
	}

	__device__ inline void getDifference(float* diff, const float* vecA, const float* vecB, float m, float n)
	{
		for (int j = 0; j < m; j++)
		{
			for (int i = 0; i < n; i++)
			{
				int idx = j * n + i;
				diff[idx] = vecA[idx] - vecB[idx];
			}
		}
	}

	__device__ inline void matmulnxn(float* result, const float* matA, const float* matB, float m, float n, float l)
	{
		for (int j = 0; j < m; j++)
		{
			for (int k = 0; k < l; k++)
			{
				for (int i = 0; i < n; i++)
				{
					int idxA = j * n + i;
					int idxB = i * l + k;
					int idxR = j * l + k;
					result[idxR] += matA[idxA] * matB[idxB];
				}
			}
		}
	}

	__device__ inline void toMatrix(const renderer::Denoiser::DenoisingTensor& tensor, float* matrix, int x, int y, int tensorLength)
	{
		for (int i = 0; i < tensorLength; ++i)
			matrix[i] = tensor.coeff(y, x, i, -1);
	}

	__device__ inline void toTensor(const float* matrix, renderer::Denoiser::DenoisingTensor& tensor, int x, int y, int tensorLength)
	{
		for (int i = 0; i < tensorLength; ++i)
			tensor.coeff(y, x, i, -1) = matrix[i];
	}

	__device__ inline void reset(renderer::Denoiser::DenoisingTensor& weights, renderer::Denoiser::DenoisingTensor& covariance,
		int x, int y, int regressorLength, bool nonlinear,
		const float3& color)
	{
		float c[] = { color.x, color.y, color.z };

		for (int i = 0; i < NUM_COLOR_CHANNELS * regressorLength; i++)
			weights.coeff(y, x, i, -1) = i % regressorLength == 0 ? c[i / regressorLength] : 0;

		for (int j = 0; j < regressorLength; j++)
			for (int i = 0; i < regressorLength; i++)
				covariance.coeff(y, x, j * regressorLength + i, -1) = i == j ? ((i > TEMPORAL_FEATURE_LENGTH && nonlinear) ? VARIANCE : VARIANCE) : 0;
	}

	__device__ inline bool safeGetValue(renderer::OutputTensor input, float3& output, int x, int y, int max_x, int max_y)
	{
		output = make_float3(0);
		if (x >= 0 && x < max_x && y >= 0 && y < max_y)
		{
			output.x = input.coeff(y, x, 0, -1);
			output.y = input.coeff(y, x, 1, -1);
			output.z = input.coeff(y, x, 2, -1);
			return true;
		}
		return false;
	}

	__device__ inline bool getTemporalFeature(
		renderer::OutputTensor currentInput, renderer::OutputTensor previousInput,
		float3& feature, float alpha,
		int x, int y, int W, int H)
	{
		float3 reprojection, up, down, left, right, middle;;

		if (safeGetValue(previousInput, reprojection, x, y, W, H))
		{
			//neighbourhood clamping
			bool clamping = safeGetValue(currentInput, middle, x, y, W, H) &&
				safeGetValue(currentInput, up, x, y + 1, W, H) &&
				safeGetValue(currentInput, down, x, y - 1, W, H) &&
				safeGetValue(currentInput, left, x + 1, y, W, H) &&
				safeGetValue(currentInput, right, x - 1, y, W, H);
			if (clamping)
			{
				float minX = min(middle.x, min(up.x, min(down.x, min(right.x, left.x))));
				float minY = min(middle.y, min(up.y, min(down.y, min(right.y, left.y))));
				float minZ = min(middle.z, min(up.z, min(down.z, min(right.z, left.z))));
				float maxX = max(middle.x, max(up.x, max(down.x, max(right.x, left.x))));
				float maxY = max(middle.y, max(up.y, max(down.y, max(right.y, left.y))));
				float maxZ = max(middle.z, max(up.z, max(down.z, max(right.z, left.z))));
				reprojection.x = min(max(reprojection.x, minX), maxX);
				reprojection.y = min(max(reprojection.y, minY), maxY);
				reprojection.z = min(max(reprojection.z, minZ), maxZ);
				/*float3 m1 = up + down + left + right + middle;
				float3 m2 = up * up + down * down + left * left + right * right + middle * middle;
				float3 mu = m1 / 5.0f;
				float3 sigma = make_float3(sqrt(m2.x / 5.0 - mu.x * mu.x), sqrt(m2.y / 5.0 - mu.y * mu.y), sqrt(m2.z / 5.0 - mu.z * mu.z));
				float gamma = 1.0f;
				float3 clippingMin = mu - sigma * gamma;
				float3 clippingMax = mu + sigma * gamma;
				reprojection.x = min(max(reprojection.x, clippingMin.x), clippingMax.x);
				reprojection.y = min(max(reprojection.y, clippingMin.y), clippingMax.y);
				reprojection.z = min(max(reprojection.z, clippingMin.z), clippingMax.z);*/


				/*float totalMin = min(minX, min(minY, minZ));
				float totalMax = max(maxX, max(maxY, maxZ));
				reprojection.x = min(max(reprojection.x, totalMin), totalMax);
				reprojection.y = min(max(reprojection.y, totalMin), totalMax);
				reprojection.z = min(max(reprojection.z, totalMin), totalMax);*/
			}

			feature = alpha * reprojection + (1 - alpha) * middle;
			return true;
		}
		return false;
	}

	__device__ inline float NLM_Weight(renderer::OutputTensor input, int idx_qx, int idx_qy, int idx_px, int idx_py, float k, float sigma_r,
		std::integral_constant<int, renderer::DenoisingSettings::DenoiseFilterMode::NLMEANS>)
	{
		float intensity_weight = 0;
		for (int n = -3; n <= 3; n++) {
			for (int m = -3; m <= 3; m++) {
				int idx_qm = idx_qx + m;
				int idx_qn = idx_qy + n;
				float3 color_q = make_float3(
					input.coeff(idx_qn, idx_qm, 0, -1),
					input.coeff(idx_qn, idx_qm, 1, -1),
					input.coeff(idx_qn, idx_qm, 2, -1)
				);
				int idx_pm = idx_px + m;
				int idx_pn = idx_py + n;
				float3 color_p = make_float3(
					input.coeff(idx_pn, idx_pm, 0, -1),
					input.coeff(idx_pn, idx_pm, 1, -1),
					input.coeff(idx_pn, idx_pm, 2, -1)
				);
				float3 colorDifference = (color_p - color_q);
				intensity_weight += dot(colorDifference, colorDifference);
			}
		}
		intensity_weight /= 147;
		intensity_weight = max(0.0f, intensity_weight - 2 * sigma_r * sigma_r) / (k * k * 2 * sigma_r * sigma_r);
	}

	__device__ inline float NLM_Weight(renderer::OutputTensor input, int idx_x, int idx_y, int x, int y, float k, float sigma_r,
		std::integral_constant<int, renderer::DenoisingSettings::DenoiseFilterMode::BILATERAL>)
	{
		float3 currentColor = make_float3(
			input.coeff(idx_y, idx_x, 0, -1),
			input.coeff(idx_y, idx_x, 1, -1),
			input.coeff(idx_y, idx_x, 2, -1)
		);
		float3 color = make_float3(
			input.coeff(y, x, 0, -1),
			input.coeff(y, x, 1, -1),
			input.coeff(y, x, 2, -1)
		);
		float3 colorDifference = (color - currentColor);
		float intensity_weight = dot(colorDifference, colorDifference) / (2 * sigma_r * sigma_r);
	}

	__global__ void DenoiseKernel(
		dim3 virtual_size,
		renderer::OutputTensor input,
		const renderer::OutputTensor previousInput,
		const renderer::DenoisingSettings s,
		renderer::Denoiser::DenoisingTensor weights,
		renderer::Denoiser::DenoisingTensor covariance)
	{
		const int H = input.rows();
		const int W = input.cols();
		const int C = input.batches();

		const int featureLength = s.feature ?
			(s.nonlinear ? TEMPORAL_FEATURE_LENGTH * NONLINEAR_FAKTOR : TEMPORAL_FEATURE_LENGTH) :
			(s.nonlinear ? GBUFFER_FEATURE_LENGTH * NONLINEAR_FAKTOR : GBUFFER_FEATURE_LENGTH);
		const int temporalFeatureLength = s.nonlinear ? TEMPORAL_FEATURE_LENGTH * NONLINEAR_FAKTOR : TEMPORAL_FEATURE_LENGTH;
		const int regressorLength = featureLength + 1;
		const int weightsLength = NUM_COLOR_CHANNELS * regressorLength;
		const int tensorLength = regressorLength * regressorLength;

		float* invCovMat = nullptr, * invCovMat_new = nullptr, * invCovMat_temp = nullptr, * beta = nullptr;
		float* diff = nullptr, * p = nullptr, * q = nullptr;
		if (s.feature)
		{
			if (s.nonlinear)
			{
				const int lengthR = TEMPORAL_FEATURE_LENGTH * NONLINEAR_FAKTOR + 1;
				const int lengthC = lengthR * lengthR;
				const int lengthW = NUM_COLOR_CHANNELS * lengthR;

				float matC[lengthC];
				invCovMat = matC;

				float matW[lengthW];
				beta = matW;

				float featureArray[TEMPORAL_FEATURE_LENGTH * NONLINEAR_FAKTOR];
				diff = featureArray;

				float regressor[lengthR];
				p = regressor;

				float qTemp[lengthR];
				q = qTemp;

				float matC1[lengthC];
				invCovMat_new = matC1;

				float matC2[lengthC];
				invCovMat_temp = matC2;
			}
			else
			{
				const int lengthC = (TEMPORAL_FEATURE_LENGTH + 1) * (TEMPORAL_FEATURE_LENGTH + 1);
				const int lengthW = NUM_COLOR_CHANNELS * (TEMPORAL_FEATURE_LENGTH + 1);

				float matC[lengthC];
				invCovMat = matC;

				float matW[lengthW];
				beta = matW;

				float featureArray[TEMPORAL_FEATURE_LENGTH];
				diff = featureArray;

				float regressor[TEMPORAL_FEATURE_LENGTH + 1];
				p = regressor;

				float qTemp[TEMPORAL_FEATURE_LENGTH + 1];
				q = qTemp;

				float matC1[lengthC];
				invCovMat_new = matC1;

				float matC2[lengthC];
				invCovMat_temp = matC2;
			}

		}
		else
		{
			if (s.nonlinear)
			{
				const int lengthR = GBUFFER_FEATURE_LENGTH * NONLINEAR_FAKTOR + 1;
				const int lengthC = lengthR * lengthR;
				const int lengthW = NUM_COLOR_CHANNELS * lengthR;

				float matC[lengthC];
				invCovMat = matC;

				float matW[lengthW];
				beta = matW;

				float featureArray[TEMPORAL_FEATURE_LENGTH * NONLINEAR_FAKTOR];
				diff = featureArray;

				float regressor[lengthR];
				p = regressor;

				float qTemp[lengthR];
				q = qTemp;

				float matC1[lengthC];
				invCovMat_new = matC1;

				float matC2[lengthC];
				invCovMat_temp = matC2;
			}
			else
			{
				const int lengthC = (GBUFFER_FEATURE_LENGTH + 1) * (GBUFFER_FEATURE_LENGTH + 1);
				const int lengthW = NUM_COLOR_CHANNELS * (GBUFFER_FEATURE_LENGTH + 1);

				float matC[lengthC];
				invCovMat = matC;

				float matW[lengthW];
				beta = matW;

				float featureArray[TEMPORAL_FEATURE_LENGTH];
				diff = featureArray;

				float regressor[GBUFFER_FEATURE_LENGTH + 1];
				p = regressor;

				float qTemp[GBUFFER_FEATURE_LENGTH + 1];
				q = qTemp;

				float matC1[lengthC];
				invCovMat_new = matC1;

				float matC2[lengthC];
				invCovMat_temp = matC2;
			}
		}

		CUMAT_KERNEL_2D_LOOP(y, x, virtual_size)
		{
			initWithZero(invCovMat, tensorLength);
			initWithZero(invCovMat_new, tensorLength);
			initWithZero(invCovMat_temp, tensorLength);
			initWithZero(beta, weightsLength);
			initWithZero(diff, featureLength);
			initWithZero(p, regressorLength);
			initWithZero(q, regressorLength);

			toMatrix(covariance, invCovMat, x, y, regressorLength * regressorLength);
			toMatrix(weights, beta, x, y, NUM_COLOR_CHANNELS * regressorLength);

			float3 color = make_float3(
				input.coeff(y, x, 0, -1),
				input.coeff(y, x, 1, -1),
				input.coeff(y, x, 2, -1)
			);
			float3 normal = make_float3(
				input.coeff(y, x, 4, -1),
				input.coeff(y, x, 5, -1),
				input.coeff(y, x, 6, -1)
			);
			float depth = input.coeff(y, x, 7, -1);
			float albedo = input.coeff(y, x, 4, -1);
			float originalGBuffer[] = { normal.x, normal.y, normal.z, depth, albedo };
			float originalColor[] = { color.x, color.y, color.z };

			float* z;
			float* original;
			float* temporal;

			float3 tempF = make_float3(0);
			if (!getTemporalFeature(input, previousInput, tempF, s.alpha, x, y, W, H)) reset(weights, covariance, x, y, regressorLength, s.nonlinear, color);

			if (s.feature)
			{
				if (s.nonlinear)
				{
					float temporalFeature[] = { tempF.x, tempF.y, tempF.z,
												log(tempF.x), log(tempF.y), log(tempF.z),
												tempF.x * tempF.x, tempF.y * tempF.y, tempF.z * tempF.z,
												tempF.x * tempF.y, tempF.y * tempF.z, tempF.z * tempF.x };
					z = temporalFeature;
					float originalFeature[] = { color.x, color.y, color.z,
												log(color.x), log(color.y), log(color.z),
												color.x * color.x, color.y * color.y, color.z * color.z,
												color.x * color.y, color.y * color.z, color.z * color.x };
					original = originalFeature;
					temporal = temporalFeature;
				}
				else
				{
					float temporalFeature[] = { tempF.x, tempF.y, tempF.z };
					z = temporalFeature;
					original = originalColor;
					temporal = temporalFeature;
				}
			}
			else
			{
				if (s.nonlinear)
				{
					float temporalFeature[] = { tempF.x, tempF.y, tempF.z,
												log(tempF.x), log(tempF.y), log(tempF.z),
												tempF.x * tempF.x, tempF.y * tempF.y, tempF.z * tempF.z,
												tempF.x * tempF.y, tempF.y * tempF.z, tempF.z * tempF.x };
					float originalFeature[] = { color.x, color.y, color.z,
												log(color.x), log(color.y), log(color.z),
												color.x * color.x, color.y * color.y, color.z * color.z,
												color.x * color.y, color.y * color.z, color.z * color.x };
					original = originalFeature;
					temporal = temporalFeature;

					float gbufferFeature[] = { normal.x, normal.x * normal.x, normal.x * normal.y,
												normal.y, normal.y * normal.y, normal.y * normal.z,
												normal.z, normal.z * normal.z, normal.z * normal.x,
												depth, depth * depth, depth * albedo,
												albedo, albedo * albedo, 1, 1, 1, 1, 1, 1 };
					z = gbufferFeature;
				}
				else
				{
					float gbufferFeature[] = { normal.x, normal.y, normal.z, depth, albedo };
					z = gbufferFeature;

					float temporalFeature[] = { tempF.x, tempF.y, tempF.z };
					original = originalColor;
					temporal = temporalFeature;
				}
			}

			//compute weight
			float w = 1.0f;
			if (s.weight)
			{
				getDifference(diff, temporal, original, temporalFeatureLength, 1);
				float d = dotProduct(diff, diff, temporalFeatureLength) / 
					(min(dotProduct(temporal, temporal, temporalFeatureLength), dotProduct(original, original, temporalFeatureLength)) +
					EPSILON);
				w = exp(-d / (s.h * s.h));
			}

			//fill regressor vector p
			p[0] = 1;
			for (int i = 1; i < regressorLength; i++) p[i] = z[i - 1];

			//compute q
			matmulnxn(q, invCovMat, p, regressorLength, regressorLength, 1);
			float denom = (s.forgettingFactor / w) + dotProduct(p, q, regressorLength);
			for (int i = 0; i < regressorLength; i++) q[i] /= denom;

			//update inverse covariance matrix
			matmulnxn(invCovMat_temp, p, invCovMat, 1, regressorLength, regressorLength);
			matmulnxn(invCovMat_new, q, invCovMat_temp, regressorLength, 1, regressorLength);
			getDifference(invCovMat_new, invCovMat, invCovMat_new, regressorLength, regressorLength);
			for (int i = 0; i < regressorLength * regressorLength; i++) invCovMat_new[i] /= s.forgettingFactor;
			toTensor(invCovMat_new, covariance, x, y, regressorLength * regressorLength);

			//update weights based on error
			float error[] = { 0, 0, 0 };
			matmulnxn(error, beta, p, NUM_COLOR_CHANNELS, regressorLength, 1);
			getDifference(error, originalColor, error, NUM_COLOR_CHANNELS, 1);
			for (int j = 0; j < NUM_COLOR_CHANNELS; j++)
			{
				for (int i = 0; i < regressorLength; i++)
				{
					int idx = j * regressorLength + i;
					beta[idx] += q[i] * error[j];
				}
			}
			toTensor(beta, weights, x, y, NUM_COLOR_CHANNELS * regressorLength);

			//compute new color
			float new_color[NUM_COLOR_CHANNELS] = { 0, 0, 0 };
			matmulnxn(new_color, beta, p, NUM_COLOR_CHANNELS, regressorLength, 1);
			color = make_float3(new_color[0], new_color[1], new_color[2]);

			input.coeff(y, x, 0, -1) = color.x;
			input.coeff(y, x, 1, -1) = color.y;
			input.coeff(y, x, 2, -1) = color.z;
		}
		CUMAT_KERNEL_2D_LOOP_END
	}

	__global__ void InitDenoisingKernel(
		dim3 virtual_size,
		renderer::Denoiser::DenoisingTensor weights,
		renderer::Denoiser::DenoisingTensor covariance,
		renderer::Denoiser::DenoisingTensor taaWeights,
		const renderer::OutputTensor currentImage,
		const int regressorLength,
		const bool nonlinear)
	{
		CUMAT_KERNEL_2D_LOOP(y, x, virtual_size)
		{
			float3 color = make_float3(
				currentImage.coeff(y, x, 0, -1),
				currentImage.coeff(y, x, 1, -1),
				currentImage.coeff(y, x, 2, -1)
			);
			reset(weights, covariance, x, y, regressorLength, nonlinear, color);
			taaWeights.coeff(y, x, 0, -1) = 1;
			taaWeights.coeff(y, x, 1, -1) = 0;
		}
		CUMAT_KERNEL_2D_LOOP_END
	}

	/**
	 * non linear means filter as a generalization of a bilateral filter
	 */
	template<int FilterMode>
	__global__ void NLMeansFilter(dim3 virtual_size, renderer::OutputTensor output, const renderer::DenoisingSettings s)
	{
		const int filter_step = s.filter_window / 2;
		int border = 0;
		if (FilterMode == renderer::DenoisingSettings::DenoiseFilterMode::NLMEANS)
		{
			border += 3;
		}

		CUMAT_KERNEL_2D_LOOP(y_, x_, virtual_size)
		{
			int x = x_ + filter_step + border;
			int y = y_ + filter_step + border;

			float3 newColor = make_float3(0);
			float weightSum = 0;

			for (int j = -filter_step; j <= filter_step; j++)
			{
				for (int i = -filter_step; i <= filter_step; i++)
				{
					int idx_x = x + i;
					int idx_y = y + j;
					float spatial_weight = ((x - idx_x) * (x - idx_x) + (y - idx_y) * (y - idx_y)) / (2 * s.sigma_s * s.sigma_s);

					float intensity_weight = NLM_Weight(output, idx_x, idx_y, x, y, s.k, s.sigma_r, std::integral_constant<int, FilterMode>());

					float weight = exp(-spatial_weight - intensity_weight);
					weightSum += weight;

					float3 currentColor = make_float3(
						output.coeff(idx_y, idx_x, 0, -1),
						output.coeff(idx_y, idx_x, 1, -1),
						output.coeff(idx_y, idx_x, 2, -1)
					);
					newColor += weight * currentColor;
				}
			}

			if (weightSum > 0) newColor /= weightSum;

			output.coeff(y, x, 0, -1) = newColor.x;
			output.coeff(y, x, 1, -1) = newColor.y;
			output.coeff(y, x, 2, -1) = newColor.z;
		}
		CUMAT_KERNEL_2D_LOOP_END
	}

	/**
	 * percentile filter
	 */
	__global__ void percentileFilter(dim3 virtual_size, renderer::OutputTensor output, const renderer::DenoisingSettings s)
	{
		const int filter_step = s.filter_window / 2;
		const int filter_size = s.filter_window * s.filter_window;

		const int MAX_FILTER_SIZE = 50;

		float values_R[MAX_FILTER_SIZE];
		float values_G[MAX_FILTER_SIZE];
		float values_B[MAX_FILTER_SIZE];

		CUMAT_KERNEL_2D_LOOP(y_, x_, virtual_size)
		{
			int x = x_ + filter_step;
			int y = y_ + filter_step;

			for (int j = -filter_step; j <= filter_step; j++)
			{
				for (int i = -filter_step; i <= filter_step; i++)
				{
					int idx = (j + filter_step) * s.filter_window + i + filter_step;
					int idx_x = x + i;
					int idx_y = y + j;

					values_R[idx] = output.coeff(idx_y, idx_x, 0, -1);
					values_G[idx] = output.coeff(idx_y, idx_x, 1, -1);
					values_B[idx] = output.coeff(idx_y, idx_x, 2, -1);
				}
			}

			//sort values using insertion sort
			for (int i = 0; i < filter_size; i++)
			{
				float current_R = values_R[i];
				float current_G = values_G[i];
				float current_B = values_B[i];

				int j = i;
				while (j > 0 && values_R[j - 1] > current_R)
				{
					values_R[j] = values_R[j - 1];
					j -= 1;
				}
				values_R[j] = current_R;

				j = i;
				while (j > 0 && values_G[j - 1] > current_G)
				{
					values_G[j] = values_G[j - 1];
					j -= 1;
				}
				values_G[j] = current_G;

				j = i;
				while (j > 0 && values_B[j - 1] > current_B)
				{
					values_B[j] = values_B[j - 1];
					j -= 1;
				}
				values_B[j] = current_B;
			}

			//compute percentile
			int idx = ceil(s.percentile * (filter_size - 1));
			float3 newColor = make_float3(values_R[idx], values_G[idx], values_B[idx]);

			output.coeff(y, x, 0, -1) = newColor.x;
			output.coeff(y, x, 1, -1) = newColor.y;
			output.coeff(y, x, 2, -1) = newColor.z;
		}
		CUMAT_KERNEL_2D_LOOP_END
	}
}

void renderer::Denoiser::initDenoising(
	renderer::Denoiser::DenoisingTensor& weights,
	renderer::Denoiser::DenoisingTensor& covariance,
	renderer::Denoiser::DenoisingTensor& taaWeights,
	const renderer::OutputTensor& currentImage,
	const int regressorLength,
	const bool nonlinear)
{
	unsigned width = weights.cols();
	unsigned height = weights.rows();
	cuMat::Context& ctx = cuMat::Context::current();
	cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(height, width, InitDenoisingKernel);
	cudaStream_t stream = ctx.stream();

	InitDenoisingKernel << < cfg.block_count, cfg.thread_per_block, 0, stream >> >
		(cfg.virtual_size,
			weights,
			covariance,
			taaWeights,
			currentImage,
			regressorLength,
			nonlinear);
	CUMAT_CHECK_ERROR();
	CUMAT_SAFE_CALL(cudaDeviceSynchronize());
}

void renderer::Denoiser::denoise(
	renderer::OutputTensor& inputTensor,
	const renderer::OutputTensor& previousInput,
	const renderer::DenoisingSettings& s,
	renderer::Denoiser::DenoisingTensor& weights,
	renderer::Denoiser::DenoisingTensor& covariance)
{
	unsigned width = inputTensor.cols();
	unsigned height = inputTensor.rows();
	cuMat::Context& ctx = cuMat::Context::current();
	cudaStream_t stream = ctx.stream();
	cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(height, width, DenoiseKernel);

	DenoiseKernel << < cfg.block_count, cfg.thread_per_block, 0, stream >> >
		(cfg.virtual_size,
			inputTensor,
			previousInput,
			s,
			weights,
			covariance);
	CUMAT_CHECK_ERROR();
	CUMAT_SAFE_CALL(cudaDeviceSynchronize());
}

void renderer::Denoiser::filter(
	renderer::OutputTensor& inputTensor,
	const renderer::DenoisingSettings& s)
{
	unsigned width = inputTensor.cols();
	unsigned height = inputTensor.rows();
	cuMat::Context& ctx = cuMat::Context::current();
	cudaStream_t stream = ctx.stream();

	int corner = s.filter_window - 1;
	if (s.filterMode == DenoisingSettings::BILATERAL)
	{
		cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(height - corner, width - corner, NLMeansFilter<DenoisingSettings::BILATERAL>);
		NLMeansFilter<DenoisingSettings::BILATERAL> << < cfg.block_count, cfg.thread_per_block, 0, stream >> >
			(cfg.virtual_size,
				inputTensor,
				s);
	}
	else if (s.filterMode == DenoisingSettings::NLMEANS)
	{
		cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(height - corner - 6, width - corner - 6, NLMeansFilter<DenoisingSettings::NLMEANS>);
		NLMeansFilter<DenoisingSettings::NLMEANS> << < cfg.block_count, cfg.thread_per_block, 0, stream >> >
			(cfg.virtual_size,
				inputTensor,
				s);
	}
	else if (s.filterMode == DenoisingSettings::PERCENTILE)
	{
		cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(height - corner, width - corner, percentileFilter);
		percentileFilter << < cfg.block_count, cfg.thread_per_block, 0, stream >> >
			(cfg.virtual_size,
				inputTensor,
				s);
	}

	CUMAT_CHECK_ERROR();
	CUMAT_SAFE_CALL(cudaDeviceSynchronize());
}
